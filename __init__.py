from trytond.pool import Pool
from . import configuration
from . import account
from . import operation
from . import operation_template
from . import party

__all__ = ['register']


def register():
    Pool.register(
        configuration.Configuration,
        account.Account,
        operation.Operation,
        operation.OperationLine,
        operation_template.OperationTemplate,
        operation_template.OperationTemplateKeyword,
        operation_template.OperationLineTemplate,
        operation_template.CreateOperationTemplate,
        operation_template.CreateOperationKeywords,
        party.Party,
        module='community_bank', type_='model')
    Pool.register(
        operation_template.CreateOperation,
        module='community_bank', type_='wizard')
    Pool.register(
        module='community_bank', type_='report')
