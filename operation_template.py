from xml.sax.saxutils import quoteattr
from decimal import Decimal

from simpleeval import simple_eval

from trytond.model import (
    ModelSQL, ModelView, DeactivableMixin, fields, sequence_ordered)
from trytond.pyson import Bool, Eval
from trytond.wizard import (Wizard, StateView, StateAction, StateTransition,
    Button)
from trytond.transaction import Transaction
from trytond.pool import Pool
from trytond.tools import decistmt


class OperationTemplate(DeactivableMixin, ModelSQL, ModelView):
    'Operation Template'
    __name__ = 'bank.operation.template'
    name = fields.Char('Name', required=True, translate=True)
    keywords = fields.One2Many('bank.operation.template.keyword', 'operation',
        'Keywords')
    company = fields.Many2One('company.company', 'Company', required=True)
    date = fields.Char('Date', help='Leave empty for today.')
    description = fields.Char('Description',
        help="Keyword values substitutions are identified "
        "by braces ('{' and '}').")
    lines = fields.One2Many('bank.operation.line.template', 'operation',
        'Lines',
        domain=['OR',
            ('account', '=', None),
            ('account.company', '=', Eval('company', -1)),
            ],
        depends=['company'])
    kind = fields.Selection(
        [
            ('normal', 'Normal'),
            ('internal', 'Internal'),
        ], 'Kind', required=True)
    sequence = fields.Integer('Sequence')

    @classmethod
    def __setup__(cls):
        super(OperationTemplate, cls).__setup__()
        cls._order = [
            ('sequence', 'ASC'),
        ]

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_kind():
        return 'normal'

    @staticmethod
    def default_sequence():
        return 1

    def get_operation(self, values):
        'Return the operation for the keyword values'
        pool = Pool()
        Operation = pool.get('bank.operation')
        Keyword = pool.get('bank.operation.template.keyword')

        operation = Operation()
        operation.company = self.company
        if self.date:
            operation.date = values.get(self.date)
        if self.description:
            operation.description = self.description.format(
                **dict(Keyword.format_values(self, values)))
        operation.lines = [l_.get_line(values) for l_ in self.lines]

        return operation


class OperationTemplateKeyword(sequence_ordered(), ModelSQL, ModelView):
    'Operation Template Keyword'
    __name__ = 'bank.operation.template.keyword'
    name = fields.Char('Name', required=True)
    string = fields.Char('String', required=True, translate=True)
    operation = fields.Many2One(
        'bank.operation.template', 'Operation', required=True)
    type_ = fields.Selection([
            ('char', 'Char'),
            ('numeric', 'Numeric'),
            ('date', 'Date'),
            ('party', 'Party'),
            ('account', 'Account'),
            ], 'Type')
    required = fields.Boolean('Required')
    active_account = fields.Boolean('Active Account',
        states={
            'invisible': Eval('type_') != 'account',
            'required': Eval('type_') == 'account',
        }, depends=['type_'])
    digits = fields.Integer('Digits',
        states={
            'invisible': Eval('type_') != 'numeric',
            'required': Eval('type_') == 'numeric',
        }, depends=['type_'])

    @staticmethod
    def default_required():
        return False

    def get_field(self):
        field = getattr(self, '_get_field_%s' % self.type_)()
        field.update({
                'name': self.name,
                'string': self.string,
                'required': self.required,
                'help': '',
                })
        return field

    def _get_field_char(self):
        return {'type': 'char'}

    def _get_field_numeric(self):
        return {'type': 'numeric', 'digits': (16, self.digits)}

    def _format_numeric(self, lang, value):
        if value:
            return lang.format('%.*f', (self.digits, value), True)
        else:
            return ''

    def _get_field_date(self):
        return {'type': 'date'}

    def _format_date(self, lang, value):
        if value:
            return lang.strftime(value)
        else:
            return ''

    def _get_field_party(self):
        return {
            'type': 'many2one',
            'relation': 'party.party',
            }

    def _format_party(self, lang, value):
        pool = Pool()
        Party = pool.get('party.party')
        if value:
            return Party(value).rec_name
        else:
            return ''

    def _get_field_account(self):
        return {
            'type': 'many2one',
            'relation': 'bank.account',
            }

    def _format_account(self, lang, value):
        pool = Pool()
        Account = pool.get('bank.account')
        if value:
            return Account(value).rec_name
        else:
            return ''

    @staticmethod
    def format_values(template, values):
        "Yield key and formatted value"
        pool = Pool()
        Lang = pool.get('ir.lang')

        lang, = Lang.search([
                ('code', '=', Transaction().language),
                ])
        keywords = {k.name: k for k in template.keywords}

        for k, v in values.items():
            keyword = keywords[k]
            func = getattr(keyword, '_format_%s' % keyword.type_, None)
            if func:
                yield k, func(lang, v)
            else:
                yield k, v


class OperationLineTemplate(ModelSQL, ModelView):
    'Operation Line Template'
    __name__ = 'bank.operation.line.template'
    operation = fields.Many2One(
        'bank.operation.template', 'Operation', required=True)
    operation_kind = fields.Selection([
            ('debit', 'Debit'),
            ('credit', 'Credit'),
            ], 'Operation Kind', required=True)
    amount = fields.Char('Amount', required=True,
        help="A python expression that will be evaluated with the keywords.")
    account = fields.Many2One('bank.account', 'Account',
        domain=[
            ('company', '=', Eval('_parent_operation', {}).get('company', -1)),
            ],
        states={
            'required': ~Bool(Eval('keyword_account')),
            'readonly': Bool(Eval('keyword_account')),
        }, depends=['keyword_account'])
    keyword_account = fields.Char(
        'Keyword Account', help="The name of the 'Account' keyword.")
    party = fields.Char('Party', help="The name of the 'Party' keyword.")
    description = fields.Char('Description',
        help="Keywords values substitutions are identified "
        "by braces ('{' and '}').")

    def get_line(self, values):
        'Return the operation line for the keyword values'
        pool = Pool()
        Line = pool.get('bank.operation.line')
        Keyword = pool.get('bank.operation.template.keyword')

        line = Line()
        amount = simple_eval(decistmt(self.amount),
            functions={'Decimal': Decimal}, names=values)
        amount = self.operation.company.currency.round(amount)
        if self.operation_kind == 'debit':
            line.debit = amount
        else:
            line.credit = amount
        if self.account:
            line.account = self.account
        elif self.keyword_account:
            line.account = values.get(self.keyword_account)
        if self.party:
            line.party = values.get(self.party)
        if self.description:
            line.description = self.description.format(
                **dict(Keyword.format_values(self.operation, values)))

        return line


class KeywordStateView(StateView):

    def get_view(self, wizard, state_name):
        fields = {}
        view = {
            'model': 'bank.operation.template.create.keywords',
            'view_id': 0,
            'type': 'form',
            'fields': fields,
            }
        if not hasattr(wizard.template, 'template'):
            return view
        template = wizard.template.template
        field_template = ('<label name=%(name)s/>'
            '<field name=%(name)s/>')
        view['arch'] = ('<?xml version="1.0"?>'
            '<form col="2" string=%s>%s</form>' % (
                quoteattr(template.name),
                ''.join(field_template % {'name': quoteattr(keyword.name)}
                    for keyword in template.keywords)
                ))
        for keyword in template.keywords:
            fields[keyword.name] = keyword.get_field()
        return view

    def get_defaults(self, wizard, state_name, fields):
        pool = Pool()
        Account = pool.get('bank.account')
        defaults = {}
        for keyword in wizard.template.template.keywords:
            if keyword.active_account:
                if isinstance(wizard.record, Account):
                    defaults[keyword.name] = wizard.record.id
        return defaults


class CreateOperation(Wizard):
    'Create Operation from Template'
    __name__ = 'bank.operation.template.create'
    start = StateTransition()
    template = StateView('bank.operation.template.create.template',
        'community_bank.operation_template_create_template_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Next', 'keywords', 'tryton-forward', default=True),
            ])
    keywords = KeywordStateView('bank.operation.template.create.keywords',
        None, [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Create', 'create_', 'tryton-ok', default=True),
            ])
    create_ = StateTransition()
    open_ = StateAction('community_bank.act_operation_from_template')

    def create_operation(self):
        template = self.template.template
        values = {}
        for keyword in template.keywords:
            values[keyword.name] = getattr(self.keywords, keyword.name, None)
        operation = template.get_operation(values)
        operation.save()
        return operation

    def transition_start(self):
        context = Transaction().context
        action_id = context.get('action_id')
        if self.model and self.model.__name__ == 'bank.operation.line':
            # Template id is used as action
            self.template.template = action_id
            return 'keywords'
        else:
            return 'template'

    def transition_create_(self):
        if self.model and self.model.__name__ == 'bank.operation.line':
            self.create_operation()
            return 'end'
        else:
            return 'open_'

    def do_open_(self, action):
        operation = self.create_operation()
        action['res_id'] = [operation.id]
        return action, {}

    def end(self):
        if self.model and self.model.__name__ == 'bank.operation.line':
            return 'reload'


class CreateOperationTemplate(ModelView):
    'Create Operation from Template'
    __name__ = 'bank.operation.template.create.template'
    template = fields.Many2One('bank.operation.template', 'Template',
        required=True,
        domain=[
            ('company', '=', Eval('context', {}).get('company', -1)),
            ])


class CreateOperationKeywords(ModelView):
    'Create Operation from Template'
    __no_slots__ = True
    __name__ = 'bank.operation.template.create.keywords'
