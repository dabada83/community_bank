# from trytond.exceptions import UserError, UserWarning
from trytond.model.exceptions import ValidationError


class AccountBalanceError(ValidationError):
    pass


class OperationBalanceError(ValidationError):
    pass
