from collections import defaultdict
from decimal import Decimal

from sql import Literal
from sql.operators import Equal

from trytond.i18n import gettext
from trytond.model import (ModelSQL, ModelView, Workflow, fields,
    DeactivableMixin, Exclude)
from trytond.pool import Pool
from trytond.pyson import If, Eval
from trytond.transaction import Transaction
from trytond.tools import grouped_slice

from .exceptions import AccountBalanceError

_DIGITS = (16, 2)
_ZERO = Decimal('0.0')


class Account(Workflow, ModelSQL, ModelView, DeactivableMixin):
    'Community Bank Account'
    __name__ = 'bank.account'
    _history = True
    _states = {
        'readonly': Eval('state') != 'draft',
        }
    _depends = ['state']

    company = fields.Many2One('company.company', 'Company', required=True,
        domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
                Eval('context', {}).get('company', -1)),
            ],
        states=_states, depends=_depends, select=True)
    code = fields.Char('Code',
        states={
            'readonly': Eval('kind') == 'normal',
        }, depends=['kind'])
    name = fields.Char('Name')
    parties = fields.One2Many(
        'party.party', 'account', 'Parties',
        states={
            'required': Eval('kind') == 'normal'
        }, depends=['kind'])
    operations = fields.One2Many(
        'bank.operation.line', 'account', 'Operations',
        states={
            'readonly': True,
        },
        filter=[
            ('operation.state', '=', 'posted'),
        ])
    balance = fields.Function(
        fields.Numeric('Balance', digits=_DIGITS), 'get_balance')
    state = fields.Selection(
        [
            ('draft', 'Draft'),
            ('open', 'Open'),
        ], 'State', required=True,
        states={
            'readonly': True
        })
    kind = fields.Selection(
        [
            ('normal', 'Normal'),
            ('internal', 'Internal'),
        ], 'Kind', required=True, states=_states, depends=_depends)

    @classmethod
    def __setup__(cls):
        super(Account, cls).__setup__()
        cls._order = [
            ('code', 'ASC'),
        ]
        cls._transitions |= set((
            ('draft', 'open'),
        ))
        t = cls.__table__()
        cls._sql_constraints = [
            ('code_exclude', Exclude(t, (t.code, Equal),
                    where=(t.active == Literal(True))
                    & (t.code != '')),
                'product.msg_account_code_unique'),
            ]
        cls._buttons.update({
            'open': {
                'invisible': ~Eval('state').in_(['draft']),
                'depends': ['state'],
            },
            'create_operation': {
                'invisible': Eval('state') == 'draft',
                'depends': ['state'],
            }
        })

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_kind():
        return 'normal'

    @classmethod
    def check_balance(cls, accounts):
        for account in accounts:
            if (account.kind == 'normal') and (account.balance < _ZERO):
                raise AccountBalanceError(
                    gettext('community_bank.account_unbalanced',
                    account=account.rec_name))

    @classmethod
    def get_balance(cls, accounts, names):
        pool = Pool()
        ids = [acc.id for acc in accounts]
        cursor = Transaction().connection.cursor()
        balances = defaultdict(lambda: _ZERO)
        OperationLine = pool.get('bank.operation.line')
        operation_line = OperationLine.__table__()
        context = Transaction().context
        posted = context.get('posted', True)
        for sub_ids in grouped_slice(ids):
            with Transaction().set_context(posted=posted, accounts_ids=sub_ids):
                query_line = OperationLine.query_get(
                    operation_line, columns=[operation_line.account])
                cursor.execute(*query_line)
                for account_id, debit, credit in cursor.fetchall():
                    balances[account_id] = debit - credit
        return {
            'balance': balances
        }

    def get_rec_name(self, name):
        return f'{self.code} - {self.name}'

    @classmethod
    def set_code(cls, accounts):
        '''
        Fill the code field with the account sequence
        '''
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Config = pool.get('bank.configuration')

        config = Config(1)
        for account in accounts:
            if account.code:
                continue
            account.code = Sequence.get_id(config.account_sequence.id)
        cls.save(accounts)

    @classmethod
    @ModelView.button
    @Workflow.transition('open')
    def open(cls, accounts):
        cls.set_code(accounts)

    @classmethod
    @ModelView.button_action('community_bank.act_operation_template_create')
    def create_operation(cls, accounts):
        pass

    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        code_value = clause[2]
        return [bool_op,
            ('code', clause[1], code_value) + tuple(clause[3:]),
            ('name',) + tuple(clause[1:]),
            ]
