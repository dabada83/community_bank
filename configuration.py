from trytond.model import ModelSQL, ModelView, ModelSingleton, fields


class Configuration(ModelSingleton, ModelSQL, ModelView):
    'Community Bank Configuration'
    __name__ = 'bank.configuration'

    account_sequence = fields.Many2One('ir.sequence', 'Accounts Sequence',
        domain=[
            ('code', '=', 'bank.account'),
        ])
    operation_sequence = fields.Many2One('ir.sequence', 'Operations Sequence',
        domain=[
            ('code', '=', 'bank.operation'),
        ])
