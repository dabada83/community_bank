from decimal import Decimal
from datetime import datetime
from collections import defaultdict

from sql import Literal
from sql.aggregate import Sum
from sql.conditionals import Coalesce

from trytond.i18n import gettext
from trytond.model import ModelSQL, ModelView, Workflow, fields
from trytond.pool import Pool
from trytond.pyson import If, Eval
from trytond.transaction import Transaction
from trytond.tools import grouped_slice, reduce_ids

from trytond.modules.company.model import employee_field, set_employee

from .exceptions import OperationBalanceError

_DIGITS = (16, 2)
_ZERO = Decimal('0.0')


class Operation(Workflow, ModelSQL, ModelView):
    'Community Bank Operation'
    __name__ = 'bank.operation'
    _history = True

    _STATES = [
        ('draft', 'Draft'),
        ('pending', 'Pending'),
        ('canceled', 'Canceled'),
        ('posted', 'Posted'),
    ]
    _states = {
        'readonly': ~(Eval('state') == 'draft')
    }
    _depends = ['state']

    company = fields.Many2One('company.company', 'Company', required=True,
        domain=[
            ('id', If(Eval('context', {}).contains('company'), '=', '!='),
                Eval('context', {}).get('company', -1)),
            ], states=_states, depends=_depends, select=True)
    number = fields.Char('Number',
        states={
            'readonly': True
        })
    date = fields.DateTime('Date',
        states={
            'readonly': True
        })
    lines = fields.One2Many('bank.operation.line', 'operation', 'Lines',
        states=_states, depends=_depends)
    debit = fields.Function(
        fields.Numeric('Debit', digits=_DIGITS), 'get_totals')
    credit = fields.Function(
        fields.Numeric('Credit', digits=_DIGITS), 'get_totals')
    state = fields.Selection(_STATES, 'State', readonly=True)
    pending_by = employee_field('Pending by')
    pending_date = fields.DateTime('Pending Date', readonly=True)
    canceled_by = employee_field('Canceled by')
    canceled_date = fields.DateTime('Canceled Date', readonly=True)
    posted_by = employee_field('Posted by')
    posted_date = fields.DateTime('Posted Date', readonly=True)
    description = fields.Text('Description')
    accounts = fields.Function(
        fields.One2Many('bank.account', None, 'Accounts'), 'get_accounts')

    @classmethod
    def __setup__(cls):
        super(Operation, cls).__setup__()
        cls._order = [
            ('number', 'ASC'),
        ]
        cls._transitions |= set((
            ('draft', 'pending'),
            ('draft', 'canceled'),
            ('pending', 'canceled'),
            ('pending', 'posted'),
        ))
        cls._buttons.update({
            'pending': {
                'invisible': ~Eval('state').in_(['draft']),
                'depends': ['state'],
                },
            'cancel': {
                'invisible': ~Eval('state').in_(['draft', 'pending']),
                'depends': ['state'],
                },
            'post': {
                'invisible': ~Eval('state').in_(['pending']),
                'depends': ['state'],
                },
        })
        cls.pending_by.states = {'readonly': True}
        cls.canceled_by.states = {'readonly': True}
        cls.posted_by.states = {'readonly': True}

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_date():
        return datetime.now()

    @staticmethod
    def default_state():
        return 'draft'

    @fields.depends('lines')
    def on_change_lines(self):
        debit = _ZERO
        credit = _ZERO
        for line in self.lines:
            debit += line.debit
            credit += line.credit
        self.debit = debit
        self.credit = credit

    @classmethod
    def check_balance(cls, operations):
        pool = Pool()
        Account = pool.get('bank.account')
        for operation in operations:
            company = operation.company
            if not company.currency.is_zero(operation.debit - operation.credit):
                raise OperationBalanceError(
                    gettext('community_bank.operation_unbalanced',
                    operation=operation.rec_name))
            with Transaction().set_context(posted=False):
                Account.check_balance(Account.browse(operation.accounts))

    @classmethod
    @ModelView.button
    @Workflow.transition('pending')
    @set_employee('pending_by')
    def pending(cls, operations):
        cls.check_balance(operations)
        for operation in operations:
            operation.pending_date = datetime.now()
        cls.save(operations)

    @classmethod
    @ModelView.button
    @Workflow.transition('canceled')
    @set_employee('canceled_by')
    def cancel(cls, operations):
        for operation in operations:
            operation.canceled_date = datetime.now()
        cls.save(operations)

    @classmethod
    @ModelView.button
    @Workflow.transition('posted')
    @set_employee('posted_by')
    def post(cls, operations):
        cls.check_balance(operations)
        for operation in operations:
            operation.posted_date = datetime.now()
        cls.save(operations)

    @classmethod
    def search_rec_name(cls, name, clause):
        code_value = clause[2]
        return [('number', clause[1], code_value) + tuple(clause[3:])]

    @classmethod
    def get_totals(cls, operations, names):
        pool = Pool()
        ids = [op.id for op in operations]
        cursor = Transaction().connection.cursor()
        debits = defaultdict(lambda: _ZERO)
        credits = defaultdict(lambda: _ZERO)
        OperationLine = pool.get('bank.operation.line')
        operation_line = OperationLine.__table__()
        for sub_ids in grouped_slice(ids):
            with Transaction().set_context(posted=False, operation_ids=sub_ids):
                query_line = OperationLine.query_get(
                    operation_line, columns=[operation_line.operation])
                cursor.execute(*query_line)
                for operation, debit, credit in cursor.fetchall():
                    debits[operation] = debit
                    credits[operation] = credit
        return {
            'debit': debits,
            'credit': credits,
        }

    @classmethod
    def get_accounts(cls, operations, names):
        pool = Pool()
        ids = [op.id for op in operations]
        cursor = Transaction().connection.cursor()
        accounts = defaultdict(lambda: [])
        OperationLine = pool.get('bank.operation.line')
        operation_line = OperationLine.__table__()
        Account = pool.get('bank.account')
        account = Account.__table__()
        table = cls.__table__()
        company = Transaction().context.get('company')
        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(table.id, sub_ids)
            query = table.join(operation_line,
                condition=operation_line.operation == table.id
            ).join(account,
                condition=operation_line.account == account.id
            ).select(
                table.id,
                operation_line.account,
                where=red_sql & (table.company == company) & (
                    table.company == account.company),
                group_by=[table.id, operation_line.account]
            )
            cursor.execute(*query)
            for operation_id, account_id in cursor.fetchall():
                accounts[operation_id].append(account_id)
        return {
            'accounts': accounts,
        }

    @classmethod
    def trigger_create(cls, operations):
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Config = pool.get('bank.configuration')
        config = Config(1)
        to_save = []
        for operation in operations:
            if not operation.number:
                operation.number = Sequence.get_id(config.operation_sequence.id)
                to_save.append(operation)
        cls.save(to_save)

    def get_rec_name(self, name):
        return f'{self.number}'


class OperationLine(ModelSQL, ModelView):
    'Community Bank Operation Line'
    __name__ = 'bank.operation.line'
    _history = True

    _states = {
        'readonly': ~(Eval('operation_state') == 'draft')
    }
    _depends = ['operation_state']

    operation = fields.Many2One('bank.operation', 'Operation', required=True,
        states={
            'readonly': True,
        })
    operation_state = fields.Function(
        fields.Selection(Operation._STATES, 'State',
            states={
                'invisible': False,
            }), 'get_operation_field')
    operation_date = fields.Function(
        fields.DateTime('Date',
            states={
                'invisible': False,
            }), 'get_operation_field')
    operation_company = fields.Function(
        fields.Many2One('company.company', 'Company',
            states={
                'invisible': True
            }), 'get_operation_field')
    account = fields.Many2One('bank.account', 'Account',
        domain=[
            ('company', '=', Eval('operation_company', -1)),
        ],
        states={
            'readonly': ~(Eval('operation_state') == 'draft')
        }, depends=_depends + ['operation_company'])
    debit = fields.Numeric('Debit', states=_states, depends=_depends)
    credit = fields.Numeric('Credit', states=_states, depends=_depends)
    party = fields.Many2One(
        'party.party', 'Party', states=_states, depends=_depends)

    @staticmethod
    def default_debit():
        return _ZERO

    @staticmethod
    def default_credit():
        return _ZERO

    @fields.depends('account', 'party')
    def on_change_account(self):
        if self.account:
            if self.account.parties:
                self.party = self.account.parties[0]

    @fields.depends('debit', 'credit')
    def on_change_debit(self):
        if self.debit:
            self.credit = _ZERO

    @fields.depends('debit', 'credit')
    def on_change_credit(self):
        if self.credit:
            self.debit = _ZERO

    @classmethod
    def query_get(cls, table, columns):
        pool = Pool()
        Operation = pool.get('bank.operation')
        operation = Operation.__table__()
        Account = pool.get('bank.account')
        account = Account.__table__()

        where_line = Literal(True)
        where_operation = Literal(True)

        context = Transaction().context
        company = context.get('company')
        if context.get('posted', True):
            where_operation &= operation.state == 'posted'
        else:
            where_operation &= operation.state.in_(
                ['draft', 'pending', 'posted'])
        if context.get('start_date'):
            where_operation &= operation.done_date >= context['start_date']
        if context.get('end_date'):
            where_operation &= operation.done_date <= context['end_date']
        if context.get('operation_ids'):
            where_line &= reduce_ids(operation.id, context.get('operation_ids'))
        if context.get('account_ids'):
            where_line &= reduce_ids(account.id, context.get('account_ids'))
        if context.get('line_ids'):
            where_line &= reduce_ids(table.id, context.get('line_ids'))

        query = table.join(account,
            condition=table.account == account.id
        ).join(operation,
            condition=table.operation == operation.id
        ).select(
            *columns,
            Sum(Coalesce(table.debit, 0)).as_('debit'),
            Sum(Coalesce(table.credit, 0)).as_('credit'),
            where=(where_line & where_operation & (
                operation.company == company) & (
                account.company == operation.company)),
            group_by=columns)
        return query

    @fields.depends('operation', '_parent_operation.state')
    def on_change_with_operation_state(self, name=None):
        if self.operation:
            return self.operation.state
        return None

    @fields.depends('operation', '_parent_operation.company')
    def on_change_with_operation_company(self, name=None):
        if self.operation:
            return self.operation.company.id
        return None

    @classmethod
    def get_operation_field(cls, lines, names):
        pool = Pool()
        ids = [li.id for li in lines]
        cursor = Transaction().connection.cursor()
        Operation = pool.get('bank.operation')
        operation = Operation.__table__()
        line = cls.__table__()
        states = defaultdict(lambda: None)
        companies = defaultdict(lambda: None)
        dates = defaultdict(lambda: None)
        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(line.id, sub_ids)
            query = line.join(operation,
                condition=line.operation == operation.id
            ).select(
                line.id,
                operation.state,
                operation.company,
                operation.posted_date,
                where=red_sql
            )
            cursor.execute(*query)
            for line_id, state, company, posted_date in cursor.fetchall():
                states[line_id] = state
                companies[line_id] = company
                dates[line_id] = posted_date

        return {
            'operation_state': states,
            'operation_company': companies,
            'operation_date': dates,
        }
