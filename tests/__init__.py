# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

try:
    from trytond.modules.community_bank.tests.test_community_bank import suite  # noqa: E501
except ImportError:
    from .test_community_bank import suite

__all__ = ['suite']
