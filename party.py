from collections import defaultdict

from trytond.model import fields
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.tools import grouped_slice, reduce_ids


class Party(metaclass=PoolMeta):
    __name__ = 'party.party'
    _history = True

    identifier = fields.Function(
        fields.Char('Identifier'), 'get_identifier')
    account = fields.Many2One(
        'bank.account', 'Account', states={'readonly': True})

    @classmethod
    def __setup__(cls):
        super(Party, cls).__setup__()
        cls.identifiers.size = 1

    @classmethod
    def get_identifier(cls, parties, names):
        table = cls.__table__()
        pool = Pool()
        cursor = Transaction().connection.cursor()
        Identifier = pool.get('party.identifier')
        identifier = Identifier.__table__()
        ids = [pa.id for pa in parties]
        result = defaultdict(lambda: None)
        for sub_ids in grouped_slice(ids):
            red_sql = reduce_ids(table.id, sub_ids)
            query = table.join(identifier,
                condition=identifier.party == table.id
            ).select(
                table.id,
                identifier.code,
                where=red_sql,
            )
            cursor.execute(*query)
            for party_id, code in cursor.fetchall():
                result[party_id] = code
        return {
            'identifier': result
        }
